#include <stdio.h>
#include "lib_lista_complementar.h"


int cria_listas(t_lista *a, t_lista *b, t_lista *c, t_lista *d){
	printf("Inicializando lista a\n");
    if (! inicializa_lista(a)) {
        printf("Erro: lista a nao foi criada.");
        return 0;
    }
	printf("Inicializando lista b\n");
    if (! inicializa_lista(b)) {
        printf("Erro: lista b nao foi criada.");
        return 0;
    }
	printf("Inicializando lista c\n");
    if (! inicializa_lista(c)) {
        printf("Erro: lista d nao foi criada.");
        return 0;
    }
	printf("Inicializando lista d\n");
    if (! inicializa_lista(d)) {
        printf("Erro: lista d nao foi criada.");
        return 0;
    }

    return 1; 
}
	
int le_lista(t_lista *a){
	int atual;

	scanf("%d", &atual);

	while(atual != 0){
		if(!insere_fim_lista(atual, a)){

			printf("Insercao falhou...\n");

			return 0;
		}

		scanf("%d", &atual);
	}

	return 1;
}

int testa_insere_lista(t_lista *b){
	int i;
	printf("Inserindo elementos na lista b. \n");
	for (i=1; i <= MAX; i++){
		if (!insere_inicio_lista(i, b)){

			printf("Insercao falhou...\n");

			return 0;
		}
	}

	return 1;
}

int teste_imprime_listas(t_lista *a, t_lista *b, t_lista *c, t_lista *d){
	
	printf("Imprimindo a lista a: \n");
	imprime_lista(a);

	printf("Imprimindo a lista b: \n");
	imprime_lista(b);

	printf("Imprimindo a lista c: \n");
	imprime_lista(c);

	printf("Imprimindo a lista d: \n");
	imprime_lista(d);

	return 1;
}

void destroi_todas_listas(t_lista *a, t_lista *b, t_lista *c, t_lista *d) {
    printf("destroi a\n");
    destroi_lista(a);
    imprime_lista(a);
    printf("destroi b\n");
    destroi_lista(b);
    imprime_lista(b);
    printf("destroi c\n");
    destroi_lista(c);
    imprime_lista(c);
    printf("destroi d\n");
    destroi_lista(d);
    imprime_lista(d);
}

int main(){
	t_lista a, b, c, d;

    if (! cria_listas(&a,&b,&c,&d) ) return 1;
    
    printf("Insira uma lista de inteiros terminada por zero: \n");
    
    if (! le_lista(&a) ) return 1;
    
    if (! (testa_insere_lista(&b)) ) return 1;

    printf("Testando copia lista de a para c. \n");
    if (! (copia_lista(&a, &c)) ){

    	printf("Erro ao copiar lista...\n");

    	return 1;
    }

    printf("Testando copia lista de b para d. \n");
    if (! (copia_lista(&b, &d)) ){

    	printf("Erro ao copiar lista...\n");

    	return 1;
    }  


    if (! teste_imprime_listas(&a,&b,&c,&d) ) return 1;

    printf("Testando concatena listas. \n");
    if (! (concatena_listas(&a,&b)) ){

    	printf("Erro ao concatenar listas...");
    	return 1;

    }

    imprime_lista(&a);

    printf("Ordenando lista a:\n");
    if(! (ordena_lista(&a)) ){

    	printf("Erro ao ordenar lista...");
    	return 1;
    }

    imprime_lista(&a);


    printf("Intercalando lista a com c: \n");

   	if(! (intercala_listas(&a, &c, &b)) ){

   		printf("Erro ao intercalar listas...");
   		return 1;

   	}

   	imprime_lista(&b);

   	printf("Testando a destruicao das listas. \n");

   	destroi_todas_listas(&a, &b, &c, &d);

    return 0;
}