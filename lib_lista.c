#include <stdio.h>
#include <stdlib.h>
#include "lib_lista.h"

int inicializa_lista(t_lista *l){
	t_nodo *sentinela_inicio=NULL, *sentinela_final=NULL;

	/* alocando espaço para as sentinelas */
	sentinela_final = (t_nodo *) malloc(sizeof(t_nodo));
	sentinela_inicio = (t_nodo *) malloc(sizeof(t_nodo));
	/* caso NULL, falta memória */
	if((sentinela_final == NULL) || (sentinela_inicio == NULL)){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}

	/*inicializa ini,fim,tam e atual da lista para as sentinelas */
	l->ini = sentinela_inicio;
	l->fim = sentinela_final;
	l->atual = NULL;
	l->tamanho = 0;

	/* aponta as sentinelas para elas mesmas, após inicado*/
	sentinela_inicio->prox = sentinela_final;
	sentinela_final->prev = sentinela_inicio;

	return 1;
}

int lista_vazia(t_lista *l){
	if(l->tamanho == 0)
		return 1;
	else
		return 0;
}

void destroi_lista(t_lista *l){
	int lixo;
	t_nodo *sentinela_inicio=NULL, *sentinela_final=NULL;


	/* checa se a lista esta vazia, se sim, retorna erro. */
	if(!lista_vazia(l)){
		while(!lista_vazia(l))
			remove_fim_lista(&lixo, l);
		
		/* agora iremos liberar as sentinelas alocadas */
		sentinela_final = l->fim;
		sentinela_inicio = l->ini;
		free(sentinela_inicio);
		free(sentinela_final);
	
	}else{
		printf("Nao destruida: lista vazia.\n");
	}

}

int insere_inicio_lista(int item, t_lista *l){
	t_nodo *novo=NULL;

	novo = (t_nodo *) malloc(sizeof(t_nodo));

	if(novo == NULL){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}


	/* configurando o novo nodo */
	novo->prox = l->ini->prox;
	novo->prev = l->ini;
	novo->chave = item;

	l->ini->prox->prev = novo;
	l->ini->prox = novo;

	l->tamanho++;

	return 1;

}

int tamanho_lista(int *tam, t_lista *l){
	*tam = l->tamanho;
	return 1;
}

int insere_fim_lista(int item, t_lista *l){
	t_nodo *novo=NULL;

	novo = (t_nodo *) malloc(sizeof(t_nodo));

	if(novo == NULL){
		printf("ERRO: Memoria insuficiente.");
		return 0;
	}

	novo->prox = l->fim;
	novo->prev = l->fim->prev;
	novo->chave = item;

	l->fim->prev->prox = novo;
	l->fim->prev = novo;

	l->tamanho++;

	return 1;
}

int insere_ordenado_lista(int item, t_lista *l){
	
	/* verifica se a lista está vazia */
	if(lista_vazia(l))
		return insere_inicio_lista(item, l);

	l->atual = l->ini->prox;
	l->fim->chave = item;
	/* caminha sobre a lista */
	while(l->atual->chave < item){
		incrementa_atual(l);
	}

	if(l->atual == l->fim)
		return insere_fim_lista(item, l);
	else if(l->atual->prev == l->ini)
		return insere_inicio_lista(item, l);
	/* caso o elemento nao pertenca nem ao inicio da lista e nem ao fim, adiciona ele atras do elemento que eh maior*/
	else{
		t_nodo *novo=NULL;
		
		novo = (t_nodo *) malloc(sizeof(t_nodo));
		
		if(novo == NULL){
			printf("ERRO: Memoria insuficiente.");
			return 0;
		}

		novo->chave = item;
		novo->prox = l->atual;
		novo->prev = l->atual->prev;
		
		l->atual->prev->prox = novo;
		l->atual->prev = novo;

		l->tamanho++;

		return 1;
	}
	return 0;
}

int remove_inicio_lista(int *item, t_lista *l){
	if(lista_vazia(l)){
		printf("Nao removido: Lista vazia! \n");
		return 0;
	}

	t_nodo *remover=NULL;
	
	remover = l->ini->prox;

	l->ini->prox->prox->prev = l->ini;
	l->ini->prox = l->ini->prox->prox;

	l->tamanho--;

	*item = remover->chave;

	free(remover);

	return 1;
}

int remove_fim_lista(int *item, t_lista *l){
	if(lista_vazia(l)){
		printf("Nao removido: Lista vazia! \n");
		return 0;
	}

	t_nodo *remover=NULL;

	remover = l->fim->prev;

	l->fim->prev->prev->prox = l->fim;
	l->fim->prev = l->fim->prev->prev;

	l->tamanho--;

	free(remover);

	return 1;

}

int remove_item_lista(int chave, int *item, t_lista *l){
	if(lista_vazia(l)){
		printf("Nao removido: Lista vazia! \n");
		return 0;
	}

	l->atual = l->ini->prox;
	l->fim->chave = chave;


	while(l->atual->chave != chave){
		incrementa_atual(l);
	}

	if(l->atual == l->fim){
		printf("Nao removido: item nao encontrado! \n");
		return 0;
	}else{
		t_nodo *remover;

		remover = l->atual;
		
		*item = l->atual->chave;

		l->atual->prev->prox = l->atual->prox;
		l->atual->prox->prev = l->atual->prev;
		l->atual = NULL;

		l->tamanho--;

		free(remover);

		return 1;		
	}

	return 0;
}


int pertence_lista(int chave, t_lista *l){
	if(lista_vazia(l)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	l->atual = l->ini->prox;
	l->fim->chave = chave;

	while(l->atual->chave != chave){
		incrementa_atual(l);
	}

	if(l->atual == l->fim)
		return 0;

	return 1;

}

int inicializa_atual_inicio(t_lista *l){
	if(lista_vazia(l)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	l->atual = l->ini->prox;

	return 1;	
}

int inicializa_atual_fim(t_lista *l){
	if(lista_vazia(l)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	l->atual = l->fim->prev;

	return 1;	
}

void incrementa_atual(t_lista *l){
	if(lista_vazia(l))
		l->atual = NULL;
	else if(l->atual == NULL)
		l->atual = l->ini->prox;
	else if(l->atual == l->fim)
		l->atual = NULL;
	else
		l->atual = l->atual->prox;

}

void decrementa_atual(t_lista *l){
	if(lista_vazia(l))
		l->atual = NULL;
	else if(l->atual == NULL)
		l->atual = NULL;
	else if(l->atual == l->ini)
		l->atual = NULL;
	else
		l->atual = l->atual->prev;
}

int consulta_item_atual(int *item, t_lista *atual){
	if(lista_vazia(atual)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	if(atual->atual == NULL)
		return 0;
	else if(atual->atual == atual->ini)
		return 0;
	else if(atual->atual  == atual->fim)
		return 0;
	else
		*item = atual->atual->chave;
	return 1;
}

int remove_item_atual(int *item, t_lista *l){
	if(lista_vazia(l)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	if(l->atual == NULL)
		return 0;
	else if((l->atual == l->ini) || (l->atual == l->ini))
		return 0;
	else{
		t_nodo *remover;

		remover = l->atual;
		*item = l->atual->chave;

		l->atual->prev->prox = l->atual->prox;
		l->atual->prox->prev = l->atual->prev;

		l->tamanho--;
		incrementa_atual(l);

		free(remover);

		return 1;
	}			
}