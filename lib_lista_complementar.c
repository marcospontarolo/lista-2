#include <stdio.h>
#include <stdlib.h>
#include "lib_lista_complementar.h"


void imprime_lista(t_lista *l){
	if(lista_vazia(l)){
		printf("Erro: Lista vazia! \n");
	}
	else{

		int item;
		int i;
		int tam_lista;

		inicializa_atual_inicio(l);
		tamanho_lista(&tam_lista,l);

		for(i = 1; i <= tam_lista; i++){

			consulta_item_atual(&item, l);
			printf(" %d", item);
			incrementa_atual(l);
			tamanho_lista(&tam_lista,l);

		}
		printf("\n");

	}
}

int copia_lista(t_lista *l, t_lista *c){

	if(lista_vazia(l)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	int i, tam_lista, item;

	inicializa_atual_inicio(l);
	tamanho_lista(&tam_lista,l);

	for(i = 1; i <= tam_lista; i++){

		consulta_item_atual(&item, l);
		insere_fim_lista(item, c);
		incrementa_atual(l);

	}

	return 1;
}

int concatena_listas(t_lista *l, t_lista *c){
	if(lista_vazia(c)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}
	int tam, item_atual, i;

	inicializa_atual_inicio(c);
	tamanho_lista(&tam,c);

	for(i = 1; i <= tam; i++){

		consulta_item_atual(&item_atual, c);
		insere_fim_lista(item_atual, l);
		incrementa_atual(c);
	}

	destroi_lista(c);

	return 1;

}

int ordena_lista(t_lista *l){
	if(lista_vazia(l)){
		printf("Erro: Lista vazia! \n");
		return 0;
	}

	t_lista aux;
	inicializa_lista(&aux);

	int i, tam, item_backup;

	tamanho_lista(&tam, l);

	for(i = 1; i <= tam; i++){

		remove_inicio_lista(&item_backup, l);
		insere_ordenado_lista(item_backup, &aux);

	}
	copia_lista(&aux, l);

	destroi_lista(&aux);

	return 1;	
}


int intercala_listas(t_lista *l, t_lista *m, t_lista *i){
	if((lista_vazia(l) && lista_vazia(m)) ){
		printf("Erro: Lista vazia! \n");
		return 0;
	}
	int tam_lista_l, tam_lista_m, item_atual_l, item_atual_m;
	
	/*
	se algumas das listas forem vazias, entao apenas resta copiar a outra para a lista i
	*/


	/* caso as duas listas possuam elementos, entao seguimos o intercala */ 
    inicializa_lista(i);
	ordena_lista(l);
	ordena_lista(m);

	inicializa_atual_inicio(l);
	inicializa_atual_inicio(m);

	tamanho_lista(&tam_lista_l, l);
	tamanho_lista(&tam_lista_m, m);

	/* enquanto as listas tiverem elementos, faz a intercalacao */
	while((tam_lista_l > 0) && (tam_lista_m > 0)){

		consulta_item_atual(&item_atual_l, l);
		consulta_item_atual(&item_atual_m, m);

		if(item_atual_l <= item_atual_m){
			insere_fim_lista(item_atual_l, i);
			incrementa_atual(l);
			tam_lista_l--;
		}

		else{
			insere_fim_lista(item_atual_m, i);
			incrementa_atual(m);
			tam_lista_m--;	

		}

	}


	/* verifica se alguma das listas ainda tem elemento */

	if(tam_lista_l > 0){
		consulta_item_atual(&item_atual_l, l);
		insere_fim_lista(item_atual_l, i);
		incrementa_atual(l);
		tam_lista_l--;
	}

	else if(tam_lista_m > 0){
		consulta_item_atual(&item_atual_m, m);
		insere_fim_lista(item_atual_m, i);
		incrementa_atual(m);
		tam_lista_m--;
	}

	return 1;


}